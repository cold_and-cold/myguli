package com.nwsuaf.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author cold_and_cold
 * @create 2021-02-17 11:08
 */
public interface WheatService {
    void getImage(MultipartFile file);
}
