package com.nwsuaf.service.impl;

import com.nwsuaf.service.WheatService;
import com.sun.corba.se.impl.orbutil.graph.Graph;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
//import org.tensorflow.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * @author cold_and_cold
 * @create 2021-02-17 11:08
 */
@Service
public class WheatServiceImpl implements WheatService {
    @Override
        public void getImage(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            InputStream inputStream = file.getInputStream();
            Image srcImage = ImageIO.read(inputStream);

            System.out.println(bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//    @Test
//    public void predict() throws Exception {
//        try(Graph graph = new Graph()){
//            graph.importGraphDef(Files.readAllBytes(Paths.get(
//                    "src/main/resources/saved_model.pb"
//            )));
////            byte[] graphBytes = IOUtils.toByteArray(new FileInputStream("src/main/resources/saved_model.pb"));
////            graph.importGraphDef(graphBytes);
//
//            try(Session session = new Session(graph)){
//                Tensor<?> out = session.runner()
//                        .feed("X",Tensor.create(2.0f))
//                        .fetch("results").run().get(0);
//                float[] r = new float[1];
//                out.copyTo(r);
//                System.out.println(r[0]);
//            }
//
//        }
//    }
}
