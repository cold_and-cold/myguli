package com.nwsuaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author cold_and_cold
 * @create 2021-02-17 10:40
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan("com.nwsuaf")
public class WheatApplication {
    public static void main(String[] args) {
        SpringApplication.run(WheatApplication.class);
    }
}
