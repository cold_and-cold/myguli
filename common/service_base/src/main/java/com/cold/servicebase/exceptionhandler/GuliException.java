package com.cold.servicebase.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author cold_and_cold
 * @create 2021-01-24 14:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GuliException extends  RuntimeException {
    private Integer code;
    private String msg;
}
