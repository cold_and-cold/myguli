package com.cold.commonutils;

import io.swagger.models.auth.In;

/**
 * @author cold_and_cold
 * @create 2021-01-23 21:10
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;
    public static Integer ERROR = 20001;
}
