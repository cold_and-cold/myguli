package com.cold.eduservice.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author cold_and_cold
 * @create 2021-01-27 20:24
 */
@Data
public class DemoData {
    // 设置Excel表头
    @ExcelProperty("学生编号")
    private Integer sno;
    @ExcelProperty("学生姓名")
    private String name;
}
