package com.cold.eduservice.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-01-27 20:26
 */
public class TestEasyExcel {
    public static void main(String[] args) {
        // 实现Excel写的操作
        String fileName = "C:\\Users\\86166\\Desktop\\寒假\\write.xlsx";
        EasyExcel.write(fileName,DemoData.class).sheet("学生列表").doWrite(getData());
    }

    private static List<DemoData> getData(){
        List<DemoData> list= new ArrayList<>();
        for (int i = 0 ;i<10 ;i++){
            DemoData d = new DemoData();
            d.setName("lucy"+i);
            d.setSno(i);
            list.add(d);
        }
        return list;
    }
}
