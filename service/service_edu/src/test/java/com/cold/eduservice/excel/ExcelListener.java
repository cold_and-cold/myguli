package com.cold.eduservice.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * @author cold_and_cold
 * @create 2021-01-27 20:35
 */
public class ExcelListener  extends AnalysisEventListener<ReadData> {

    // 一行一行读取内容
    @Override
    public void invoke(ReadData readData, AnalysisContext analysisContext) {
        System.out.println("___"+readData);

    }

    // 读取表头
    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头");
    }

    // 读完之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
