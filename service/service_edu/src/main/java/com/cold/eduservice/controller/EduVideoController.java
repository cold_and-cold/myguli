package com.cold.eduservice.controller;


import com.cold.commonutils.R;
import com.cold.eduservice.client.VodClient;
import com.cold.eduservice.entity.EduVideo;
import com.cold.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
@RestController
@RequestMapping("/eduservice/edu-video")
public class EduVideoController {
    @Autowired
    private EduVideoService eduVideoService;

    @PostMapping("addVideo")
    public R addVideo(@RequestBody EduVideo eduVideo){
        eduVideoService.save(eduVideo);
        return R.ok();
    }

    // TODO 需要删除视频
    @DeleteMapping("deleteVideo/{videoId}")
    public R deleteVideo(@PathVariable String videoId){
        eduVideoService.removeVideoAndInfo(videoId);
        return R.ok();
    }

    @GetMapping("getVideo/{videoId}")
    public R getVideoById(@PathVariable String  videoId){
        EduVideo eduVideo = eduVideoService.getById(videoId);
        return  R.ok().data("eduVideo",eduVideo);
    }

    @PostMapping("updateVideo")
    public R updateVideo(@RequestBody EduVideo eduVideo){
        eduVideoService.updateById(eduVideo);
        return R.ok();
    }



}

