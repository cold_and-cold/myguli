package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
