package com.cold.eduservice.controller;


import com.cold.commonutils.R;
import com.cold.eduservice.entity.subject.MySubject;
import com.cold.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-01-27
 */
@RestController
@RequestMapping("/eduservice/edu-subject")
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    // 添加课程分类
    // 获取上传文件，把文件中的内容读取
    @PostMapping("addSubject")
    public R addSubject(MultipartFile file){
        eduSubjectService.saveSubject(file,eduSubjectService);
        return R.ok();
    }

    @GetMapping("getAllSubject")
    public R getAllSubject(){
        List<MySubject> allSubject= eduSubjectService.getAllSubject();
        return  R.ok().data("items",allSubject);
    }


}

