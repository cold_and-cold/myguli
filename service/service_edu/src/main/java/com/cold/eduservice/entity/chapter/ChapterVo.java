package com.cold.eduservice.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-01-28 21:39
 */
@Data
public class ChapterVo {
    private String id;
    private String title;

    private List<VideiVo> children = new ArrayList<>();
}
