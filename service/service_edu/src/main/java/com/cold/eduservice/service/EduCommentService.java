package com.cold.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author cold
 * @since 2021-02-09
 */
public interface EduCommentService extends IService<EduComment> {

    HashMap<String, Object> getPageComment(Page<EduComment> page, String courseId);
}
