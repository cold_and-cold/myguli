package com.cold.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cold.eduservice.client.VodClient;
import com.cold.eduservice.entity.EduVideo;
import com.cold.eduservice.mapper.EduVideoMapper;
import com.cold.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {
    @Autowired
    private VodClient vodClient;



    @Override
    public List<EduVideo> getAllVideo(String id) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",id);
        List<EduVideo> eduVideos = baseMapper.selectList(wrapper);
        return eduVideos;
    }

    @Override
    public void deleteCourseVideoAly(List<EduVideo> list) {
        for(EduVideo eduVideo : list){
            String videoSourceId = eduVideo.getVideoSourceId();
            if(!StringUtils.isEmpty(videoSourceId)){
                vodClient.removeAlyVideo(videoSourceId);
            }
        }
    }

    @Override
    public void removeVideoAndInfo(String videoId) {
        EduVideo eduVideo = baseMapper.selectById(videoId);
        String videoSourceId = eduVideo.getVideoSourceId();
        if(!StringUtils.isEmpty(videoSourceId)){
            vodClient.removeAlyVideo(videoSourceId);
        }
        baseMapper.deleteById(videoId);
    }

    @Override
    public void removeVideoByCourseId(String id) {
        QueryWrapper<EduVideo>wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",id);
        wrapper.select("video_source_id");
        List<EduVideo> eduVideos = baseMapper.selectList(wrapper);

    }

}
