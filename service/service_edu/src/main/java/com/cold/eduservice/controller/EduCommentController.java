package com.cold.eduservice.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.JwtUtils;
import com.cold.commonutils.R;
import com.cold.eduservice.client.UcenterClient;
import com.cold.eduservice.entity.EduComment;
import com.cold.eduservice.service.EduCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-09
 */
@RestController
@RequestMapping("/eduservice/edu-comment")
public class EduCommentController {
    @Autowired
    private EduCommentService commentService;

    @Autowired
    private UcenterClient ucenterClient;

    @PostMapping("addComment")
    public R addComment(@RequestBody EduComment comment, HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        if(StringUtils.isEmpty(memberId)) {
            return R.error().code(28004).message("请登录");
        }
        HashMap<String, Object> memberInfo = ucenterClient.eduGetMember(memberId);
        System.out.println(memberId);
        System.out.println(memberInfo);
        comment.setAvatar((String) memberInfo.get("avatar"));
        comment.setMemberId((String)memberInfo.get("memberId"));
        comment.setNickname((String)memberInfo.get("nickName"));
        commentService.save(comment);
        return R.ok();
    }


    @GetMapping("getAllCommentByPage/{current}/{limit}")
    public R getAllCommentByPage(@PathVariable long current,
                                 @PathVariable long limit,
                                 String courseId){
        Page<EduComment> page = new Page<>(current,limit);
        HashMap<String,Object> map = commentService.getPageComment(page,courseId);
        return R.ok().data("map",map);

    }

}

