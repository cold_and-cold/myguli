package com.cold.eduservice.client;

import com.alibaba.excel.event.Order;
import org.springframework.stereotype.Component;

/**
 * @author cold_and_cold
 * @create 2021-02-14 16:07
 */
@Component
public class OrderFallbackClient implements OrderClient {
    @Override
    public boolean queryIsBuy(String memberId, String courseId) {
        return false;
    }
}
