package com.cold.eduservice.client;

import com.cold.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-02-03 10:57
 */
@Component
public class VodFileDegradeFeignClient implements  VodClient{
    @Override
    public R removeAlyVideo(String id) {
        return R.error().message("time out");
    }

    @Override
    public R deleteBatch(List<String> videoList) {
        return R.error().message("time out");
    }
}
