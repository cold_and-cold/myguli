package com.cold.eduservice.entity.subject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-01-28 10:35
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MySubject {
    private String id;
    private String title;
    List<MySubject> children;
}
