package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-27
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
