package com.cold.eduservice.service;

import com.cold.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cold.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduChapterService extends IService<EduChapter> {

    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    boolean removeChapterById(String chapterId);
}
