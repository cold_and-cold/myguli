package com.cold.eduservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author cold_and_cold
 * @create 2021-02-14 16:06
 */
@Component
@FeignClient(name = "service-order" )
public interface OrderClient {
    @GetMapping("/eduorder/order/queryIsBuy/{memberId}/{courseId}")
    public boolean queryIsBuy(@PathVariable("memberId") String memberId,
                              @PathVariable("courseId") String  courseId);
}
