package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-09
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
