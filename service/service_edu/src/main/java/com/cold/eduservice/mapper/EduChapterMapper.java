package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
