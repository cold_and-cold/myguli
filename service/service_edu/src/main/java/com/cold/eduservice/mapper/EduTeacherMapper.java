package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-23
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
