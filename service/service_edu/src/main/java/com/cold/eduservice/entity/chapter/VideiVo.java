package com.cold.eduservice.entity.chapter;

import lombok.Data;

/**
 * @author cold_and_cold
 * @create 2021-01-29 10:34
 */
@Data
public class VideiVo {
    private String id;
    private String title;
    private String videoSourceId;
}
