package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cold.eduservice.entity.vo.CourseFrontInfoVo;
import com.cold.eduservice.entity.vo.CoursePublishVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    CoursePublishVo getCoursePublishVoById(String id);
    CourseFrontInfoVo selectInfoWebById(String id);
}
