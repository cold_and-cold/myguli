package com.cold.eduservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.R;
import com.cold.commonutils.entity.CourseInfoUtils;
import com.cold.eduservice.entity.EduCourse;
import com.cold.eduservice.entity.vo.CourseFrontInfoVo;
import com.cold.eduservice.entity.vo.CourseInfoVo;
import com.cold.eduservice.entity.vo.CoursePublishVo;
import com.cold.eduservice.entity.vo.CourseQuery;
import com.cold.eduservice.service.EduChapterService;
import com.cold.eduservice.service.EduCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
@RestController
@RequestMapping("/eduservice/edu-course")
public class EduCourseController {

    @Autowired
    private EduCourseService eduCourseService;

    // 添加课程基本信息
    @PostMapping("addCourseInfo")
    public R addCourseInfo(@RequestBody CourseInfoVo courseInfoVo){
        String courseId = eduCourseService.saveCourseInfo(courseInfoVo);
        return  R.ok().data("courseId",courseId);
    }

    @GetMapping("getCourseInfo/{courseId}")
    public  R getCourseInfo(@PathVariable String courseId){
        CourseInfoVo courseInfoVo = eduCourseService.getCourseInfo(courseId);
        return R.ok().data("courseInfoVo",courseInfoVo);
    }

    @PostMapping("updateCourseInfo")
    public R updateCourseInfo(@RequestBody CourseInfoVo courseInfoVo){
        eduCourseService.updateCourseInfoVo(courseInfoVo);
        return R.ok();
    }

    @GetMapping("getCoursePublishInfo/{id}")
    public R getCoursePublishInfo(@PathVariable String id){
        CoursePublishVo coursePublishVo = eduCourseService.getPublishCourseInfo(id);
        return R.ok().data("items",coursePublishVo);
    }

    @GetMapping("publishCourse/{courseId}")
    public R publishCourse(@PathVariable String courseId){
        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(courseId);
        eduCourse.setStatus("Normal");
        boolean b = eduCourseService.updateById(eduCourse);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @GetMapping("getAllCourse")
    public  R getAllCourse(){
        List<EduCourse> list = eduCourseService.list(null);
        return R.ok().data("items",list);
    }

    @PostMapping("getCourseByCondition/{current}/{limit}")
    public R getCourseByCondition(@PathVariable long current,
                                  @PathVariable long limit,
                                  @RequestBody CourseQuery courseQuery){
        Page<EduCourse> page = new Page<>(current,limit);
        IPage<EduCourse> eduCourseIPage = eduCourseService.getCourseByCondition(page,courseQuery);
        long total = eduCourseIPage.getTotal();
        List<EduCourse> records = eduCourseIPage.getRecords();
        return R.ok().data("total",total).data("list",records);
    }

    @GetMapping("getCourseByPage/{current}/{limit}")
    public R getCourseByPage(@PathVariable long current ,
                             @PathVariable long limit){
        Page<EduCourse> page = new Page<>(current,limit);
        IPage<EduCourse> eduCourseIPage = eduCourseService.page(page, null);
        long total = eduCourseIPage.getTotal();
        List<EduCourse> records = eduCourseIPage.getRecords();
        return R.ok().data("total",total).data("list",records);
    }

    @GetMapping("deleteCourseById/{courseId}")
    public R deleteCourseById(@PathVariable String courseId){
        eduCourseService.removeCourseInfoById(courseId);
        return R.ok();
    }

    @GetMapping("remoteGetCourse/{courseId}")
    public CourseInfoUtils remoteGetCourse(@PathVariable String courseId){
        CourseFrontInfoVo vo = eduCourseService.selectCourseInfo(courseId);
        CourseInfoUtils courseInfoUtils = new CourseInfoUtils();
        BeanUtils.copyProperties(vo,courseInfoUtils);
        return courseInfoUtils;
    }



}

