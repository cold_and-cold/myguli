package com.cold.eduservice.controller;

import com.cold.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author cold_and_cold
 * @create 2021-01-26 11:34
 */
@RestController
@RequestMapping("/eduservice/user")
public class EduLoginController {
    //login
    @PostMapping("login")
    public R login(){
        return R.ok().data("token","admin");
    }

    @GetMapping("info")
    public R info(){
        return  R.ok().data("roles","[admin]").data("name","admin").data("avatar","");
    }

}
