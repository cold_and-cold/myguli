package com.cold.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.R;
import com.cold.eduservice.entity.EduTeacher;
import com.cold.eduservice.entity.vo.TeacherQuery;
import com.cold.eduservice.service.EduTeacherService;
import com.cold.servicebase.exceptionhandler.GuliException;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import sun.rmi.runtime.Log;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-01-23
 */


@RestController
@RequestMapping("/eduservice/edu-teacher")
public class EduTeacherController {
    @Autowired
    private EduTeacherService eduTeacherService;


    @ApiOperation(value = "修改")
    @PostMapping("updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
        boolean b = eduTeacherService.updateById(eduTeacher);
        return b? R.ok():R.error();
    }

    @ApiOperation(value = "通过id查讲师")
    @GetMapping("getTeacher/{id}")
    public R getTeacherById(@PathVariable Long id){
        EduTeacher byId = eduTeacherService.getById(id);
        return R.ok().data("teacher",byId);

    }

    @ApiOperation(value = "添加讲师")
    @PostMapping("addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher){
        boolean save = eduTeacherService.save(eduTeacher);
        if(save){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation(value = "获取所有教师")
    @GetMapping("findAll")
    public R findAllTeacher(){
        List<EduTeacher> list = eduTeacherService.list(null);

        return R.ok().data("list",list);
    }



    @ApiOperation(value = "逻辑删除通过id")
    @DeleteMapping("deleteById/{id}")
    public R removeTeacherById(@PathVariable String id){
        boolean b = eduTeacherService.removeById(id);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation(value = "获取讲师分页")
    @GetMapping("pageTeacher/{current}/{limit}")
    public R pageList(
            @ApiParam(value = "当前页" ,required = true)
            @PathVariable Long current,
            @ApiParam(value = "每页的大小",required = true)
            @PathVariable Long limit
            ){
        Page<EduTeacher> page = new Page<>(current,limit);

        eduTeacherService.page(page,null);
        List<EduTeacher> records = page.getRecords();
        long total = page.getTotal();
        return R.ok().data("total",total).data("records",records);
    }
    @ApiOperation(value = "通过条件获取讲师分页")
    @PostMapping("pageTeacherByCondition/{current}/{limit}")
    public R pageListByCondition(
            @PathVariable long current,
            @PathVariable long limit,
            @RequestBody(required = false) TeacherQuery teacherQuery
    ){
        Page<EduTeacher> pageQuery = new Page<>(current,limit);
        eduTeacherService.queryTeacherPageByCondition(pageQuery,teacherQuery);
        long total = pageQuery.getTotal();
        List<EduTeacher> records = pageQuery.getRecords();
        return R.ok().data("total",total).data("rows",records);
    }


}

