package com.cold.eduservice.service;

import com.cold.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cold.eduservice.entity.subject.MySubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-27
 */
public interface EduSubjectService extends IService<EduSubject> {

    void saveSubject(MultipartFile file, EduSubjectService eduSubjectService);

    List<MySubject> getAllSubject();
}
