package com.cold.eduservice.client;

import com.cold.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author cold_and_cold
 * @create 2021-02-09 19:29
 */
@Component
@FeignClient(name = "service-ucenter" ,fallback = UcenterFallbackClient.class)
public interface UcenterClient {
    @GetMapping(value = "/educenter/ucenter-member/eduGetMember/{id}")
    public HashMap<String, Object> eduGetMember(@PathVariable("id") String id);
}
