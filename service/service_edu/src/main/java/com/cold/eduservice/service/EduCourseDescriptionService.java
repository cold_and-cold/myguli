package com.cold.eduservice.service;

import com.cold.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
