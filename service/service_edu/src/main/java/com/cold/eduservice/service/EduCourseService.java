package com.cold.eduservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cold.eduservice.entity.vo.*;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduCourseService extends IService<EduCourse> {

    String saveCourseInfo(CourseInfoVo courseInfoVo);

    CourseInfoVo getCourseInfo(String courseId);

    void updateCourseInfoVo(CourseInfoVo courseInfoVo);

    CoursePublishVo getPublishCourseInfo(String id);


    IPage<EduCourse> getCourseByCondition(Page<EduCourse> page, CourseQuery courseQuery);

    void removeCourseInfoById(String courseId);


    List<EduCourse> getFrontCourse();

    List<EduCourse> getCourseByTeacherId(String id);

    HashMap<String, Object> getFrontCourseByQuerVo(Page<EduCourse> page, FronCourseQueryVo courseQueryVo);

    CourseFrontInfoVo selectCourseInfo(String id);

    void updateCourseView(String id);

    Long getCourseViewCount(String id);

    void updateCourseViewCountScheduled(String courseId, String viewCount);
}
