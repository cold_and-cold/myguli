package com.cold.eduservice.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.R;
import com.cold.eduservice.entity.EduCourse;
import com.cold.eduservice.entity.EduTeacher;
import com.cold.eduservice.service.EduCourseService;
import com.cold.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-02-07 15:08
 */
@RequestMapping("eduservice/edu-teacher-front")
@RestController
public class TeacherFrontController {
    @Autowired
    private EduTeacherService eduTeacherService;
    @Autowired
    private EduCourseService eduCourseService;
    @GetMapping("getTeacherInfor/{id}")
    public R getTeacherInfor(@PathVariable String id){
        List<EduCourse> list =  eduCourseService.getCourseByTeacherId(id);
        EduTeacher teacher = eduTeacherService.getById(id);

        return R.ok().data("teacher",teacher).data("list",list);
    }

    @PostMapping("getTeacherFrontList/{page}/{limit}")
    public R getTeacherFrontList(
            @PathVariable long page,
            @PathVariable long limit
    ){
        Page<EduTeacher> page1 = new Page<>(page,limit);
        HashMap<String ,Object> map = eduTeacherService.querTeacherFront(page1);
        return R.ok().data("map",map);
    }

}
