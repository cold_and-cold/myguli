package com.cold.eduservice.service;

import com.cold.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduVideoService extends IService<EduVideo> {
    List<EduVideo> getAllVideo(String id);
    void deleteCourseVideoAly(List<EduVideo> list);

    void removeVideoAndInfo(String videoId);

    void removeVideoByCourseId(String id);
}
