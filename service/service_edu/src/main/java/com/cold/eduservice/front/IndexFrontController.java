package com.cold.eduservice.front;

import com.cold.commonutils.R;
import com.cold.eduservice.entity.EduCourse;
import com.cold.eduservice.entity.EduTeacher;
import com.cold.eduservice.service.EduCourseService;
import com.cold.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-02-03 17:47
 */
@RestController
@RequestMapping("/eduservice/edu-front")
public class IndexFrontController {
    @Autowired
    private EduCourseService eduCourseService;
    @Autowired
    private EduTeacherService eduTeacherService;

    @GetMapping("getIndexCourse")
    public R getIndexCourse(){
        List<EduCourse> list =  eduCourseService.getFrontCourse();

        return R.ok().data("items",list);
    }

    @GetMapping("getIndexTeacher")
    public R getIndexTeacher(){
        List<EduTeacher> list = eduTeacherService.getFrontTeacher();
        return R.ok().data("items",list);
    }
}
