package com.cold.eduservice.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cold.eduservice.entity.vo.TeacherQuery;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author cold
 * @since 2021-01-23
 */
public interface EduTeacherService extends IService<EduTeacher> {

    void queryTeacherPageByCondition(Page<EduTeacher> pageQuery, TeacherQuery teacherQuery);

    List<EduTeacher> getFrontTeacher();

    HashMap<String, Object> querTeacherFront(Page<EduTeacher> page1);
}
