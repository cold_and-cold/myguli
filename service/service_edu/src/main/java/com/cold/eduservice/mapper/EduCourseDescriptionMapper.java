package com.cold.eduservice.mapper;

import com.cold.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
