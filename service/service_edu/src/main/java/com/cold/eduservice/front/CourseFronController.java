package com.cold.eduservice.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.JwtUtils;
import com.cold.commonutils.R;
import com.cold.eduservice.client.OrderClient;
import com.cold.eduservice.entity.EduCourse;
import com.cold.eduservice.entity.chapter.ChapterVo;
import com.cold.eduservice.entity.chapter.VideiVo;
import com.cold.eduservice.entity.vo.CourseFrontInfoVo;
import com.cold.eduservice.entity.vo.FronCourseQueryVo;
import com.cold.eduservice.service.EduChapterService;
import com.cold.eduservice.service.EduCourseService;
import net.bytebuddy.asm.Advice;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-02-07 19:43
 */
@RestController
@RequestMapping("eduservice/edu-course-front")
public class CourseFronController {
    @Autowired
    private EduCourseService eduCourseService;

    @Autowired
    private EduChapterService eduChapterService;

    @Autowired
    private OrderClient orderClient;

    @PostMapping("getPageCourse/{current}/{limit}")
    public R getPageCourse(@PathVariable long current,
                           @PathVariable long limit,
                           @RequestBody FronCourseQueryVo courseQueryVo){
        Page<EduCourse> page = new Page<>(current,limit);
        HashMap<String ,Object> map = eduCourseService.getFrontCourseByQuerVo(page,courseQueryVo);

        return R.ok().data("map",map);
    }

    @GetMapping("getCourseInfo/{id}")
    public R getCourseInfo(@PathVariable String  id, HttpServletRequest request){
        //eduCourseService.updateCourseView(id);
        CourseFrontInfoVo vo =  eduCourseService.selectCourseInfo(id);
        List<ChapterVo> chapterVideoByCourseId = eduChapterService.getChapterVideoByCourseId(id);
        boolean isBuy = orderClient.queryIsBuy("1358300626236260354", id);
        //String s  =  JwtUtils.getMemberIdByJwtToken(request);
        //System.out.println(s+"===================");
        System.out.println(request);
        System.out.println(isBuy);
        return R.ok().data("courseInfo",vo).data("list",chapterVideoByCourseId).data("isBuy",isBuy);
    }

}
