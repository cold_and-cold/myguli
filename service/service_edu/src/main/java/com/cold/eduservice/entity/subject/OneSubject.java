package com.cold.eduservice.entity.subject;

import lombok.Data;

/**
 * @author cold_and_cold
 * @create 2021-01-28 10:34
 */
@Data
public class OneSubject {
    private String id;
    private String name;
}
