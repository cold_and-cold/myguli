package com.cold.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.eduservice.client.VodClient;
import com.cold.eduservice.entity.EduChapter;
import com.cold.eduservice.entity.EduCourse;
import com.cold.eduservice.entity.EduCourseDescription;
import com.cold.eduservice.entity.EduVideo;
import com.cold.eduservice.entity.vo.*;
import com.cold.eduservice.mapper.EduCourseMapper;
import com.cold.eduservice.service.EduChapterService;
import com.cold.eduservice.service.EduCourseDescriptionService;
import com.cold.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cold.eduservice.service.EduVideoService;
import com.cold.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author cold
 * @since 2021-01-28
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService eduCourseDescriptionService;

    @Autowired
    private EduChapterService eduChapterService;
    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    private VodClient vodClient;

    @Autowired
    private RedisTemplate<String,String> template ;

    @Override
    public String saveCourseInfo(CourseInfoVo courseInfoVo) {

        EduCourse eduCourse= new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if(insert <= 0){
            throw  new GuliException(20001,"添加课程失败");
        }
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        eduCourseDescription.setId(eduCourse.getId());
        eduCourseDescriptionService.save(eduCourseDescription);

        return eduCourse.getId();
    }

    @Override
    public CourseInfoVo getCourseInfo(String courseId) {
        System.out.println(courseId);

        EduCourse eduCourse = baseMapper.selectById(courseId);
        EduCourseDescription eduCourseDescription = eduCourseDescriptionService.getById(courseId);
        CourseInfoVo courseInfoVo = new CourseInfoVo();
        BeanUtils.copyProperties(eduCourse,courseInfoVo);
        courseInfoVo.setDescription(eduCourseDescription.getDescription());
        return courseInfoVo;
    }

    @Override
    public void updateCourseInfoVo(CourseInfoVo courseInfoVo) {
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        boolean b = this.updateById(eduCourse);
        if(!b){
            throw new GuliException(20001,"修改课程信息失败");

        }
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        eduCourseDescriptionService.updateById(eduCourseDescription);
    }

    @Override
    public CoursePublishVo getPublishCourseInfo(String id) {
        CoursePublishVo publishCourseInfo = baseMapper.getCoursePublishVoById(id);
        return publishCourseInfo;
    }



    @Override
    public IPage<EduCourse> getCourseByCondition(Page<EduCourse> page, CourseQuery courseQuery) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        String subjectId = courseQuery.getSubjectId();
        String subjectParentId = courseQuery.getSubjectParentId();
        String teacherId = courseQuery.getTeacherId();
        String title = courseQuery.getTitle();
        if(!StringUtils.isEmpty(subjectId)){
            wrapper.eq("subject_id",subjectId);
        }
        if(!StringUtils.isEmpty(subjectParentId)){
            wrapper.eq("subject_parent_id",subjectParentId);
        }
        if(!StringUtils.isEmpty(teacherId)){
            wrapper.eq("teacher_id",teacherId);
        }
        if(!StringUtils.isEmpty(title)){
            wrapper.eq("title",title);
        }

        IPage<EduCourse> eduCourseIPage = baseMapper.selectPage(page, wrapper);


        return eduCourseIPage;
    }

    @Override
    public void removeCourseInfoById(String courseId) {
        QueryWrapper<EduCourse> wrapper1 = new QueryWrapper<>();
        QueryWrapper<EduVideo> wrapper2 = new QueryWrapper<>();
        QueryWrapper<EduChapter> wrapper3 = new QueryWrapper<>();
        QueryWrapper<EduCourseDescription> wrapper4 = new QueryWrapper<>();
        wrapper1.eq("id",courseId);
        wrapper2.eq("course_id",courseId);
        wrapper3.eq("course_id",courseId);
        wrapper4.eq("id",courseId);
        List<EduVideo> allVideo = eduVideoService.getAllVideo(courseId);
        eduVideoService.deleteCourseVideoAly(allVideo);
        boolean remove = eduVideoService.remove(wrapper2);
        if(!remove){
            throw new GuliException(20001,"删除课程异常");
        }
        remove = eduChapterService.remove(wrapper3);
        if(!remove){
            throw new GuliException(20001,"删除课程异常");
        }
        remove = eduCourseDescriptionService.remove(wrapper4);
        if(!remove){
            throw new GuliException(20001,"删除课程异常");
        }
        int delete = baseMapper.delete(wrapper1);
        if(delete <=0){
            throw new GuliException(20001,"删除课程异常");
        }
    }

    @Cacheable(value = "course",key="'frontCourse'")
    @Override
    public List<EduCourse> getFrontCourse() {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 8");
        List<EduCourse> list = baseMapper.selectList(wrapper);

        return list;
    }

    @Override
    public List<EduCourse> getCourseByTeacherId(String id) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id",id);
        List<EduCourse> list = baseMapper.selectList(wrapper);
        return list;
    }

    @Override
    public HashMap<String, Object> getFrontCourseByQuerVo(Page<EduCourse> pageParam, FronCourseQueryVo courseQueryVo) {
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        String buyCountSort = courseQueryVo.getBuyCountSort();
        String gmtCreateSort = courseQueryVo.getGmtCreateSort();
        String priceSort = courseQueryVo.getPriceSort();
        String subjectId = courseQueryVo.getSubjectId();
        String subjectParentId = courseQueryVo.getSubjectParentId();
        String teacherId = courseQueryVo.getTeacherId();
        String title = courseQueryVo.getTitle();
        if(!StringUtils.isEmpty(buyCountSort)){
            wrapper.orderByDesc("buy_count");
        }
        if(!StringUtils.isEmpty(gmtCreateSort)){
            wrapper.orderByDesc("gmt_create");
        }
        if(!StringUtils.isEmpty(priceSort)){
            wrapper.orderByDesc("price");
        }
        if(!StringUtils.isEmpty(subjectId)){
            wrapper.eq("subject_id",subjectId);
        }
        if(!StringUtils.isEmpty(subjectParentId)){
            wrapper.eq("subject_parent_id",subjectParentId);
        }
        if(!StringUtils.isEmpty(teacherId)){
            wrapper.eq("teacher_id",teacherId);
        }
        if(!StringUtils.isEmpty(title)){
            wrapper.eq("title",title);
        }
        baseMapper.selectPage(pageParam,wrapper);
        HashMap<String ,Object> map = new HashMap<>();
        List<EduCourse> records = pageParam.getRecords();
        long current = pageParam.getCurrent();
        long pages = pageParam.getPages();
        long size = pageParam.getSize();
        long total = pageParam.getTotal();
        boolean hasNext = pageParam.hasNext();
        boolean hasPrevious = pageParam.hasPrevious();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);
        return map;
    }

    @Override
    public CourseFrontInfoVo selectCourseInfo(String id) {
        System.out.println("调用==========================================");
        Long viewCount = this.getCourseViewCount(id);
        CourseFrontInfoVo vo = baseMapper.selectInfoWebById(id);

        if(viewCount==null){
            template.opsForValue().set("viewCount::" + id, String.valueOf(vo.getViewCount()+1));
        }else{
            vo.setViewCount(viewCount);
        }

        return vo;
    }

    @Override
    public void updateCourseView(String id) {
        EduCourse eduCourse = baseMapper.selectById(id);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhhhhhhhhh==================+1");
        eduCourse.setViewCount(eduCourse.getViewCount()+1);
        baseMapper.updateById(eduCourse);
    }

    @Override
    public Long getCourseViewCount(String id) {
        String s = template.opsForValue().get("viewCount::" + id);
        if(s!=null){
            Long viewCount = Long.valueOf(s);
            if(viewCount!=null){

            }
            Long increment = template.opsForValue().increment("viewCount::" + id);
            return increment;
        }

        return null;
    }

    @Override
    public void updateCourseViewCountScheduled(String courseId, String viewCount) {
        EduCourse eduCourse = baseMapper.selectById(courseId);
        eduCourse.setViewCount(Long.valueOf(viewCount));
        baseMapper.updateById(eduCourse);

    }


}
