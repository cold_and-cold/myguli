package com.cold.msmservice.service;

import java.util.Map;

/**
 * @author cold_and_cold
 * @create 2021-02-04 11:10
 */
public interface MsmService {
     boolean send(String PhoneNumbers, String templateCode, Map<String,Object> param);
}
