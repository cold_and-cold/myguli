package com.cold.eduorder.controller;


import com.cold.commonutils.R;
import com.cold.eduorder.service.PayLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
@RestController
@RequestMapping("/eduorder/pay-log")
public class PayLogController {
    @Autowired
    private PayLogService payLogService;

    @GetMapping("createNative/{orderNo}")
    public R createNative(@PathVariable String  orderNo){
        Map map =  payLogService.createNative(orderNo);

        return R.ok().data("map",map);
    }

    @GetMapping("queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable String orderNo){
        Map<String ,String > map = payLogService.queryPayStatus(orderNo);
        if(map == null){
            return R.error();
        }
        if (map.get("trade_state").equals("SUCCESS")) {//如果成功
            //更改订单状态
            payLogService.updateOrderStatus(map);
            return R.ok().message("支付成功");
        }
        return R.ok();

    }
}

