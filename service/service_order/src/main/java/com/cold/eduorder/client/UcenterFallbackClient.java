package com.cold.eduorder.client;

import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author cold_and_cold
 * @create 2021-02-09 19:30
 */
@Component
public class UcenterFallbackClient implements UcenterClient{
    @Override
    public HashMap<String, Object> eduGetMember(String id) {
        return null;
    }
}
