package com.cold.eduorder.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cold.commonutils.JwtUtils;
import com.cold.commonutils.R;
import com.cold.eduorder.entity.Order;
import com.cold.eduorder.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
@RestController
@RequestMapping("/eduorder/order")

public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("createOrder/{courseId}")
    public R createOrder(@PathVariable String courseId, HttpServletRequest request){
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        String orderId = orderService.createOrder(courseId,memberId);
        return R.ok().data("orderId",orderId);
    }

    @GetMapping("getOrderByNo/{orderNo}")
    public R getOrderByNo(@PathVariable String orderNo){
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",orderNo);
        Order order = orderService.getOne(wrapper);
        return R.ok().data("items",order);

    }

    @GetMapping("queryIsBuy/{memberId}/{courseId}")
    public boolean queryIsBuy(@PathVariable String memberId,
                              @PathVariable String  courseId){
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("member_id", memberId);
        wrapper.eq("course_id", courseId);
        wrapper.eq("status", 1);
        int count = orderService.count(wrapper);
        if(count>0){
            return true;
        }else {
            return false;
        }

    }

}

