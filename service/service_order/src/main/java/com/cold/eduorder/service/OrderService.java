package com.cold.eduorder.service;

import com.cold.eduorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
public interface OrderService extends IService<Order> {

    String createOrder(String courseId, String memberId);
}
