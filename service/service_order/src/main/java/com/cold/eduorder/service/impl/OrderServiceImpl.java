package com.cold.eduorder.service.impl;

import com.cold.commonutils.entity.CourseInfoUtils;
import com.cold.eduorder.client.EduClient;
import com.cold.eduorder.client.UcenterClient;
import com.cold.eduorder.entity.Order;
import com.cold.eduorder.mapper.OrderMapper;
import com.cold.eduorder.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cold.eduorder.utils.OrderNoUtil;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
    @Autowired
    private UcenterClient ucenterClient;
    @Autowired
    private EduClient eduClient;


    @Override
    public String createOrder(String courseId, String memberId) {
        CourseInfoUtils courseInfo = eduClient.remoteGetCourse(courseId);
        HashMap<String, Object> map = ucenterClient.eduGetMember(memberId);

        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseInfo.getTitle());
        order.setCourseCover(courseInfo.getCover());
        order.setTeacherName(courseInfo.getTeacherName());
        order.setTotalFee(courseInfo.getPrice());
        order.setMemberId(memberId);
        order.setMobile((String) map.get("mobile"));
        order.setNickname((String) map.get("nickName"));
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);

        return order.getOrderNo();
    }
}
