package com.cold.eduorder.client;

import com.cold.commonutils.entity.CourseInfoUtils;
import org.springframework.stereotype.Component;

/**
 * @author cold_and_cold
 * @create 2021-02-14 11:00
 */
@Component
public class EduFallbackClient implements EduClient {

    @Override
    public CourseInfoUtils remoteGetCourse(String courseId) {
        return null;
    }
}
