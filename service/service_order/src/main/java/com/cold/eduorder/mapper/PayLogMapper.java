package com.cold.eduorder.mapper;

import com.cold.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
