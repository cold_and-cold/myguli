package com.cold.eduorder.mapper;

import com.cold.eduorder.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
public interface OrderMapper extends BaseMapper<Order> {

}
