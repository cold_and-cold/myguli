package com.cold.eduorder.client;

import com.cold.commonutils.entity.CourseInfoUtils;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author cold_and_cold
 * @create 2021-02-14 10:58
 */
@Component
@FeignClient(name = "service-edu" ,fallback = EduFallbackClient.class)
public interface EduClient {
    @GetMapping("/eduservice/edu-course/remoteGetCourse/{courseId}")
    public CourseInfoUtils remoteGetCourse(@PathVariable("courseId") String courseId);
}
