package com.cold.educms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author cold_and_cold
 * @create 2021-02-03 16:58
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.cold"})
@MapperScan("com.cold.educms.mapper")
public class CmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class);
    }
}
