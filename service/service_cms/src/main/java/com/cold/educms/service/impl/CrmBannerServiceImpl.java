package com.cold.educms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cold.educms.entity.CrmBanner;
import com.cold.educms.mapper.CrmBannerMapper;
import com.cold.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author cold
 * @since 2021-02-03
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    @Cacheable(value = "banner",key="'selectIndexList'")
    @Override
    public List<CrmBanner> getBannerList() {
        QueryWrapper<CrmBanner> wrapper  = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 3");
        List<CrmBanner> crmBanners = baseMapper.selectList(wrapper);
        return crmBanners;
    }
}
