package com.cold.educms.service;

import com.cold.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author cold
 * @since 2021-02-03
 */
public interface CrmBannerService extends IService<CrmBanner> {

    List<CrmBanner> getBannerList();
}
