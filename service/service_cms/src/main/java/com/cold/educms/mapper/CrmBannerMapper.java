package com.cold.educms.mapper;

import com.cold.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-03
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
