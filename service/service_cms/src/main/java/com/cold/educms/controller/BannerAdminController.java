package com.cold.educms.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.R;
import com.cold.educms.entity.CrmBanner;
import com.cold.educms.service.CrmBannerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author cold_and_cold
 * @create 2021-02-03 17:17
 */
@RestController
@RequestMapping("/educms/crm-banner/admin")
public class BannerAdminController {
    @Autowired
    private CrmBannerService crmBannerService;


    @GetMapping("pageBanner/{current}/{limit}")
    public R getPageBanner(@PathVariable long current, @PathVariable long  limit){
        Page<CrmBanner> page = new Page<>(current,limit);
        IPage<CrmBanner> page1 = crmBannerService.page(page, null);
        long total = page1.getTotal();
        List<CrmBanner> records = page1.getRecords();
        return R.ok().data("total",total).data("items",records);
    }

    @ApiOperation(value = "获取Banner")
    @GetMapping("get/{id}")
    public R get(@PathVariable String id) {
        CrmBanner banner = crmBannerService.getById(id);
        return R.ok().data("item", banner);
    }

    @ApiOperation(value = "新增Banner")
    @PostMapping("save")
    public R save(@RequestBody CrmBanner banner) {
        crmBannerService.save(banner);
        return R.ok();
    }

    @ApiOperation(value = "修改Banner")
    @PutMapping("update")
    public R updateById(@RequestBody CrmBanner banner) {
        crmBannerService.updateById(banner);
        return R.ok();
    }

    @ApiOperation(value = "删除Banner")
    @DeleteMapping("remove/{id}")
    public R remove(@PathVariable String id) {
        crmBannerService.removeById(id);
        return R.ok();
    }
}
