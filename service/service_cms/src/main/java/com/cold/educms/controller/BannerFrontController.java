package com.cold.educms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cold.commonutils.R;
import com.cold.educms.entity.CrmBanner;
import com.cold.educms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-03
 */
@RestController
@RequestMapping("/educms/crm-banner/front")
public class BannerFrontController {
    @Autowired
    private CrmBannerService crmBannerService;

    @GetMapping("getAllBanner")
    public R getAllBanner(){
        List<CrmBanner> list = crmBannerService.getBannerList();
        return R.ok().data("items",list);
    }



}

