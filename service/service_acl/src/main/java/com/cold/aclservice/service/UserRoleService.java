package com.cold.aclservice.service;

import com.cold.aclservice.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserRoleService extends IService<UserRole> {

}
