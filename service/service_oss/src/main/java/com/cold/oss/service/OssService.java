package com.cold.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author cold_and_cold
 * @create 2021-01-27 16:37
 */
public interface OssService {
    String uploadFileAvatar(MultipartFile file);
}
