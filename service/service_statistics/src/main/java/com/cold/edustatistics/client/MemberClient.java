package com.cold.edustatistics.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author cold_and_cold
 * @create 2021-02-14 20:22
 */
@Component
@FeignClient(name = "service-ucenter")
public interface MemberClient {
    @GetMapping("/educenter/ucenter-member/getCountRegister/{day}")
    public Integer getCountRegister(@PathVariable("day") String day);
}
