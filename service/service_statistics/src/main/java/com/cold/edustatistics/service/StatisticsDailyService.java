package com.cold.edustatistics.service;

import com.cold.edustatistics.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {

    void getCountRegister(String day);

    Map<String, Object> showData(String type, String begin, String end);
}
