package com.cold.edustatistics.controller;


import com.cold.commonutils.R;
import com.cold.edustatistics.service.StatisticsDailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
@RestController
@RequestMapping("/edustatistics/statistics-daily")
@CrossOrigin
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService dailyService;

    @PostMapping("registerCount/{day}")
    public R registerCount(@PathVariable String day){
        dailyService.getCountRegister(day);
        return R.ok();
    }

    @GetMapping("showData/{type}/{begin}/{end}")
    public R showData(@PathVariable String type ,
                    @PathVariable String begin,
                    @PathVariable String end){
        Map<String,Object> map = dailyService.showData(type,begin,end);
        return R.ok().data(map);
    }

}

