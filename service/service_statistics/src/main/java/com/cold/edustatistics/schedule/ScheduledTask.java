package com.cold.edustatistics.schedule;

import com.cold.edustatistics.service.StatisticsDailyService;
import com.cold.edustatistics.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author cold_and_cold
 * @create 2021-02-15 12:07
 */
@Component

public class ScheduledTask {
    @Autowired
    private StatisticsDailyService dailyService;

    // 开启定时的任务，下面的cron的表达式是每隔五秒执行一次
    @Scheduled(cron = "0 0 1 * * ?")
    public void task2() {
        //获取上一天的日期
        String day = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));
        dailyService.getCountRegister(day);

    }



}
