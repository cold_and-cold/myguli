package com.cold.edustatistics.mapper;

import com.cold.edustatistics.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-14
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
