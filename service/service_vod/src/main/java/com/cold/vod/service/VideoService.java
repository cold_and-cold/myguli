package com.cold.vod.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author cold_and_cold
 * @create 2021-02-02 11:36
 */
public interface VideoService {
    String uploadVideo(MultipartFile file);

    void deleteVideo(String id);
}
