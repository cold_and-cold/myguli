package com.cold.educenter.mapper;

import com.cold.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author cold
 * @since 2021-02-05
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

    Integer selectCountRegister(String day);
}
