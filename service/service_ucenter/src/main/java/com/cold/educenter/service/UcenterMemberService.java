package com.cold.educenter.service;

import com.cold.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cold.educenter.entity.vo.RegisterVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author cold
 * @since 2021-02-05
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember member);

    boolean register(RegisterVo registerVo);

    UcenterMember getByOpenId(String openid);

    Integer counRegister(String day);
}
