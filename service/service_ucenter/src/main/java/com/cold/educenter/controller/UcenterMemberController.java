package com.cold.educenter.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cold.commonutils.JwtUtils;
import com.cold.commonutils.R;
import com.cold.educenter.entity.UcenterMember;
import com.cold.educenter.entity.vo.RegisterVo;
import com.cold.educenter.service.UcenterMemberService;
import com.cold.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author cold
 * @since 2021-02-05
 */
@RestController
@RequestMapping("/educenter/ucenter-member")
public class UcenterMemberController {
    @Autowired
    private UcenterMemberService memberService;


    @PostMapping("login")
    public R login(@RequestBody UcenterMember member){
        System.out.println("testtttttt=============");
        String token = memberService.login(member);
        return R.ok().data("token",token);
    }
    
    @PostMapping("register")
    public R register(@RequestBody RegisterVo registerVo){
        boolean flag = memberService.register(registerVo);
        if(flag){
            return R.ok();
        }else {
            return R.error();
        }
    }
    // 根据登录返回的token获取用户信息
    @GetMapping("getMemberInfo")
    public R getMemberInfo(HttpServletRequest request){
        String id = JwtUtils.getMemberIdByJwtToken(request);
        UcenterMember member = memberService.getById(id);
        System.out.println("test++++++++++++++++++");
        return R.ok().data("userInfo",member);
    }
    @GetMapping("eduGetMember/{id}")
    public HashMap<String, Object> eduGetMember(@PathVariable String id){
        UcenterMember member = memberService.getById(id);
        HashMap<String ,Object> map = new HashMap<>();
        map.put("avatar",member.getAvatar());
        map.put("memberId",member.getId());
        map.put("nickName",member.getNickname());
        map.put("mobile",member.getMobile());
        System.out.println(map);
        return map;
    }

    @GetMapping("getCountRegister/{day}")
    public Integer getCountRegister(@PathVariable String day){
        Integer count = memberService.counRegister(day);
        return count;
    }



}

