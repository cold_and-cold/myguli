package com.cold.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cold.commonutils.JwtUtils;
import com.cold.commonutils.MD5;
import com.cold.educenter.entity.UcenterMember;
import com.cold.educenter.entity.vo.RegisterVo;
import com.cold.educenter.mapper.UcenterMemberMapper;
import com.cold.educenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cold.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author cold
 * @since 2021-02-05
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {
    @Autowired
    private RedisTemplate<String ,String> template;

    @Override
    public String login(UcenterMember member) {
        String mobile = member.getMobile();
        String password = member.getPassword();
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new GuliException(20001,"登录失败");
        }

        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        if(null == ucenterMember){
            throw new GuliException(20001,"手机号不存在");
        }

        String encrypt = MD5.encrypt(password);
        if(!encrypt.equals(ucenterMember.getPassword())){
            throw new GuliException(20001,"密码错误");
        }
        if(ucenterMember.getIsDisabled()){
            throw new GuliException(20001,"该账号被禁用");
        }
        String token = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());


        return token;
    }

    @Override
    public boolean register(RegisterVo registerVo) {
        String code = registerVo.getCode(); // 验证码
        String mobile = registerVo.getMobile(); // 手机号
        String nickname = registerVo.getNickname(); // 昵称
        String password = registerVo.getPassword(); // 密码
        if(StringUtils.isEmpty(code) || StringUtils.isEmpty(mobile) ||
                StringUtils.isEmpty(nickname)|| StringUtils.isEmpty(password)) {

        }
        String redisCode = template.opsForValue().get(mobile);
        if(!code.equals(redisCode)){
            throw new GuliException(20001,"验证码错误");
        }
        QueryWrapper<UcenterMember>wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer integer = baseMapper.selectCount(wrapper);
        if(integer>0){
            throw new GuliException(20001,"该账号已存在");
        }
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        String encrypt = MD5.encrypt(password);
        member.setPassword(encrypt);
        member.setIsDisabled(false);
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");

        baseMapper.insert(member);

        return true;
    }

    @Override
    public UcenterMember getByOpenId(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    @Override
    public Integer counRegister(String day) {
        Integer count = baseMapper.selectCountRegister(day);
        return count;
    }
}
